#ifndef __TIME_EVENTS_H__
#define __TIME_EVENTS_H__

#include <time.h>


typedef struct {
  unsigned int min;
  unsigned int sec;
  unsigned int hun;
} time_decomp;


// For both functions, the unsigned int is the number of hundredth of seconds corresponding
unsigned int time_decomp_to_int(time_decomp time);
// Should be useless
time_decomp int_to_time_decomp(unsigned int time);


void wait_hundredth_seconds(unsigned int length);


#endif
