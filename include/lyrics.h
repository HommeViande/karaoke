#ifndef __LYRICS_H__
#define __LYRICS_H__

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifndef TEXT
#include <cairo.h>
#include <X11/Xlib.h>
#include <cairo-xlib.h>
#define WINSIZEX 900
#define WINSIZEY 600
#endif

#include "time_events.h"
#include "lrc_info.h"
#include <unistd.h>

typedef struct {
  char text[1024];
  unsigned int length;
} lyrics_line;

typedef struct {
  lyrics_line* text;
  int length;
} song;

// The input string must be a markup of type line
lyrics_line text_to_line(char* l);
song init_song(FILE* lyrics);
#ifdef TEXT
void display_line(lyrics_line l);
#else
void display_line(cairo_surface_t *surface, lyrics_line l);
#endif
void display(song s);

#endif
