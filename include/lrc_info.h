#ifndef __LRC_INFO_H__
#define __LRC_INFO_H__


// To read files
#include <stdio.h>
// To make life with strings a little easier
#include <string.h>



typedef enum {
              ar,
              al,
              ti,
              au,
              length,
              by,
              offset,
              re,
              ve,
              line,
              empty
} markup;


static inline unsigned char is_a_number(char c) { return c <= '9' && c >= '0'; }


markup get_markup(char* line);
void get_markup_content(char* l, char* buffer);

void get_artist(FILE* lyrics, char* artist);
void get_album(FILE* lyrics, char* album);
void get_title(FILE* lyrics, char* title);
void get_length(FILE* lyrics, char* length);


#endif
