#include "lrc_info.h"



markup get_markup(char* l)
{
  int i, li, lipu;
  //for (i=0; l[i] != ' ' && l[i] != '[' && l[i] != '\0'; i++);
  for (i=0; l[i] != '[' && l[i] != '\0'; i++);
  if (l[i] == '\0') return empty;
  li = l[i+1];
  lipu = l[i+2];
  switch (li)
    {
    case 'a':
      if (lipu == 'r')
        return ar;
      else if (lipu == 'l')
        return al;
      else if (lipu == 'u')
        return au;
      else
        return empty;
      break;
    case 'b':
      if (lipu == 'y')
        return by;
      else
        return empty;
      break;
    case 'l':
      if (lipu == 'e' && l[i+3] == 'n' && l[i+4] == 'g' && l[i+5] == 't' && l[i+6] == 'h')
        return length;
      else
        return empty;
      break;
    case 'o':
      if (lipu == 'f' && l[i+3] == 'f' && l[i+4] == 's' && l[i+5] == 'e' && l[i+6] == 't')
        return offset;
      else
        return empty;
      break;
    case 'r':
      if (lipu == 'e')
        return re;
      else
        return empty;
      break;
    case 't':
      if (lipu == 'i')
        return ti;
      else
        return empty;
      break;
    case 'v':
      if (lipu == 'e')
        return ve;
      else
        return empty;
      break;
    }
  // Checking if it's a correct time
  if (is_a_number(l[i+1]) && is_a_number(l[i+2]) && l[i+3] == ':' && is_a_number(l[i+4]) && is_a_number(l[i+5]) && l[i+6] == '.' && is_a_number(l[i+7]) && is_a_number(l[i+8]))
    return line;
  return empty;
}

void get_markup_content(char* l, char* buffer)
{
  markup m = get_markup(l);
  size_t t;
  l++;
  switch(m)
    {
    case ar:
    case al:
    case ti:
    case au:
    case by:
    case re:
    case ve:
      l+=2;
      break;
    case line:
      l+=9;
      break;
    case offset:
    case length:
      l+=6;
      break;
    case empty:
      buffer=0;
      return;
    }
  for(;*l == ' ' || *l == ':'; l++);
  if (m != line)
    for(t=strlen(l); l[t-1] == '\r' || l[t-1] == ']' || l[t-1] == ' '; t--);
  else
    t=strlen(l);
  l[t] = '\0';
  strcpy(buffer, l);
}

void get_artist(FILE* lyrics, char* artist)
{
  int found=0, error=0;
  char l[1024];
  markup m;;
  do
    {
      error = fscanf(lyrics, "%[^\n]\n", l) == EOF;
      m = get_markup(l);
      found = m == ar;
    } while (!found && !error);
  if (found)
    get_markup_content(l,artist);
  else
    strcpy(artist, "Unknown");
  rewind(lyrics);
}

void get_album(FILE* lyrics, char* album)
{
  int found=0, error=0;
  char l[1024];
  markup m;
  do
    {
      error = fscanf(lyrics, "%[^\n]\n", l) == EOF;
      m = get_markup(l);
      found = m == al;
    } while (!found && !error);
  if (found)
    get_markup_content(l,album);
  else
    strcpy(album, "Unknown");
  rewind(lyrics);
}

void get_title(FILE* lyrics, char* title)
{
  int found=0, error=0;
  char l[1024];
  markup m;
  do
    {
      error = fscanf(lyrics, "%[^\n]\n", l) == EOF;
      m = get_markup(l);
      found = m == ti;
    } while (!found && !error);
  if (found)
    get_markup_content(l,title);
  else
    strcpy(title, "Unknown");
  rewind(lyrics);
}

void get_length(FILE* lyrics, char* length)
{
  int found=0, error=0;
  char l[1024];
  markup m;
  do
    {
      error = fscanf(lyrics, "%[^\n]\n", l) == EOF;
      m = get_markup(l);
      found = m == al;
    } while (!found && !error);
  if (found)
    get_markup_content(l,length);
  else
    strcpy(length, "Unknown");
  rewind(lyrics);
}
