#include "time_events.h"


unsigned int time_decomp_to_int(time_decomp time)
{
  return time.hun + 100 * time.sec + 6000 * time.min;
}

time_decomp int_to_time_decomp(unsigned int time)
{
  time_decomp t;
  t.hun = time % 100;
  t.sec = (time % 6000) - t.hun;
  t.min = time - t.sec - t.hun;
  return t;
}


void wait_hundredth_seconds(unsigned int length)
{
  clock_t arrivee=clock()+((length*CLOCKS_PER_SEC)/100); // The moment it has to stop waiting
  while(clock()<arrivee);
}
