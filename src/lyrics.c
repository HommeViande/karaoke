#include "lyrics.h"

lyrics_line text_to_line(char* l)
{
  lyrics_line ll;
  unsigned int i, size;
  for (i=0; !is_a_number(l[i]); i++);
  size = (l[i+7]-'0') + 10*(l[i+6]-'0') + 100*(l[i+4]-'0') + 1000*(l[i+3]-'0') + 6000*(l[i+1]-'0') + 60000*(l[i]-'0');
  get_markup_content(l, ll.text);
  ll.length = size;
  return ll;
}


song init_song(FILE* lyrics)
{
  song s;
  unsigned int song_size=0;
  int end_of_file=0;
  char l[1024];
  int i;
  // Here we look for the size of the song
  while (!end_of_file)
    {
      end_of_file = fscanf(lyrics, "%[^\n]\n", l) == EOF;
      song_size += get_markup(l) == line;
    }
  s.length = song_size-1;
  s.text = malloc(song_size*sizeof(lyrics_line));
  // Here we fill the song
  end_of_file=0; i=0; rewind(lyrics);
  while (!end_of_file)
    {
      end_of_file = fscanf(lyrics, "%[^\n]\n", l) == EOF;
      if(get_markup(l) == line)
        {
          s.text[i] = text_to_line(l);
          i++;
        }
    }
  return s;
}


#ifdef TEXT
void display_line(lyrics_line l)
{
  printf("%s\n", l.text);
}

void display(song s)
{
  int i;
  printf("\n");
  wait_hundredth_seconds(s.text[0].length);
  for(i=0; i<s.length; i++)
    {
      display_line(s.text[i]);
      wait_hundredth_seconds(s.text[i+1].length - s.text[i].length);
      printf("\e[A");
      printf("\e[2K");
    }
}


#else
void display_line(cairo_surface_t *surface, lyrics_line l)
{
  cairo_t *cr;
	cr=cairo_create(surface);
  cairo_set_source_rgb(cr, 0, 0, 0);
  cairo_paint(cr);
  cairo_set_source_rgb(cr, 1., 1., 1.);
  cairo_select_font_face(cr, "Hacker", CAIRO_FONT_SLANT_NORMAL,CAIRO_FONT_WEIGHT_BOLD);
  cairo_set_font_size(cr, 13);
  cairo_move_to(cr, 620, 30);
  char text[255];
  sprintf(text, "%s", l.text);
  cairo_show_text(cr, text);
  cairo_destroy(cr);
  printf("%s\n", text);
}

void display(song s)
{
  // X11 display
	Display *dpy;
	Window rootwin;
	Window win;
	XEvent e;
	int scr;
  // init the display
	if(!(dpy=XOpenDisplay(NULL))) {
		fprintf(stderr, "ERROR: Could not open display\n");
		exit(1);
	}
	scr=DefaultScreen(dpy);
	rootwin=RootWindow(dpy, scr);
  win=XCreateSimpleWindow(dpy, rootwin, 1, 1, WINSIZEX, WINSIZEY, 0, BlackPixel(dpy, scr), BlackPixel(dpy, scr));
  XStoreName(dpy, win, "Karaoke");
	//XSelectInput(dpy, win, KeyPressMask|ButtonPressMask|ExposureMask);
	XMapWindow(dpy, win);
	// create cairo surface
	cairo_surface_t *cs;
	cs=cairo_xlib_surface_create(dpy, win, DefaultVisual(dpy, 0), WINSIZEX, WINSIZEY);
  cairo_t *cr;
  cr=cairo_create(cs);
  cairo_set_source_rgb (cr, 0.0, 0.0, 1.0);
  //cairo_paint(cr);
  //cairo_fill(cr);
  cairo_rectangle(cr,30,30,50,50);
	cairo_set_source_rgb (cr, 0.0, 1.0, 0.0);
	cairo_fill(cr);
  cairo_destroy(cr);
  int i;
  printf("\n");
  lyrics_line l = {"Test putain", 200};
  display_line(cs, l);
  //wait_hundredth_seconds(s.text[0].length);
  //usleep(s.text[0].length*10000);
  for(i=0; i<s.length; i++)
    {
      display_line(cs, s.text[i]);
      //wait_hundredth_seconds(s.text[i+1].length - s.text[i].length);
      usleep((s.text[i+1].length - s.text[i].length)*10000);
      //XNextEvent(dpy, &e);
      //printf("\e[A");
      //printf("\e[2K");
      }
  cairo_surface_destroy(cs); // destroy cairo surface
  XCloseDisplay(dpy); // close the display
}
#endif
